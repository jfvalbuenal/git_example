# Introducción al uso de GIT para el versionamiento de información espacial

[Enlace al ejercicio](https://jfvalbuenal.gitlab.io/git_example/)

El repositorio conserva el código implementado para introducir al uso de **GIT** como sistema de control de versiones, de manera adicional presenta un ejemplo para el versionamiento de información espacial en formato *shapefile* usando este mismo sistema.

La información geográfica con que se desarrolla el ejercicio esta dispuesta en [este enlace](https://drive.google.com/file/d/1qN6cCh-glnwxKVkVz3SYl-edpNFMTMLT/view)

Actualmente el ejercicio no contempla el uso de un gestor de diseño específico para el versionamiento de información espacial, sin embargo se contempla una segunda entrega que guíe el uso de [**Kart**](https://docs.kartproject.org/en/latest/pages/quick_guide.html#installing) para el versionamiento de información espacial en diferentes formatos.