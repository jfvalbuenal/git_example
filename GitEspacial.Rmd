---
title: "Uso de GIT para el versionamiento en datos espaciales: \n ejercicio práctico"
author: "John Fredy Valbuena Lozano"
date: ""
output:
  rmdformats::downcute:
    highlight: monochrome

---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(markdown)
```

```{bash, include=F}
rm -rf Carpeta_Ejercicio_Git  
```

 


# ¿Qué es `GIT` y por qué usarlo?

[`Git`](https://git-scm.com/) es un sistema de control de versiones que permite generar un seguimiento a los cambios realizados sobre uno o más archivos, 
evitando así que sea necesario guardar múltiples versiones para mantener el control sobre las modificaciones que se realizan.

Adicionalmente permite trabajar con múltiples usuarios que, en su flujo de trabajo, interactúan con un archivo común.
Esta característica hace posible identificar quien y en qué momento generó una modificación sobre un archivo que está siendo modificado por un equipo de trabajo.

Sumado a las características anteriores, `Git` hace posible generar flujos de trabajo paralelos,los cuales son manipulados como **ramas** en donde modificaciones especificas que se llevan a cabo por diferentes personas, pueden ser realizadas en paralelo y, una vez el resultado esté en optimas condiciones, puede ser fusionado al producto final supervisado.

![**Figura 1**: Flujo de trabajo en `git` usando ramas](Imagenes/git-branches-merge.png)

Todo aquello que involucre versiones puede ser indexado en `Git` para su optimo seguimiento colaborativo.

`Git` requiere de incorporar una terminología fundamental para comprender su funcionamiento, para ello será ideal tener en cuenta los siguientes términos o instrucciones:

 - Repositorio
 - *Commit*
 - *Branch*
 - *Merge*
 - *Checkout*
 
A medida que el taller avance estos términos cobrarán el sentido que requieren para el trabajo al interior de `Git`.

## Descarga y primeros pasos

`Git` es un software libre y abierto, creado en 2005 por *Linus Torvalds* con el propósito de ofrecer una alternativa para el seguimiento de cambios en archivos, estas características permiten descargar y usar el software sin restricciones, siendo esto válido para sistemas operativos como **Windows**, **Mac** o **GNU-Linux**.

Ahora bien, es necesario tener en cuenta que, contrario a la mayoría de programas que solemos utilizar, al descargar `Git` tendremos que operarlo a través de una *Terminal* o *Línea de comandos*.

![**Figura 2**: Línea de comandos `Git` en Windows](Imagenes/Terminal.PNG)


### Instalación en Windows

La instalación se realiza de manera muy sencilla, solo hace falta descargar el instalador y ejecutarlo en su maquina local, para ello la página oficial dispone de un [link de descargas](https://git-scm.com/downloads), allí debe ubicar en qué sistema operativo desea instalar el programa, si es el caso especificar la arquitectura y descargar.


![**Figura 3**: Instalador `Git` en Windows](Imagenes/InstaladorWindows.PNG)


# Contexto y usos comunes de`Git` ¿Cómo trabaja?

El uso de `Git` está fuertemente relacionado con el desarrollo informático, sea este web, de software u otros,
esto explica el por qué su uso es ampliamente difundido entre quienes trabajan con archivos de texto plano o **scripts** creados para ser ejecutados por determinados lenguajes de programación.

Varias son las características de `Git` que le han convertido en una herramienta muy bien valorada a la hora de trabajar en equipo, es por ello que para su correcta comprensión y, antes de iniciar labores sobre su interfaz, es necesario reconocer de manera general, las particularidades que le definen.

## De folder a respositorio

`Git` transforma la carpeta en la que almacenamos nuestros archivos en un repositorio de seguimiento mediante una instrucción sencilla, la orden de inicio. Para ello en primer lugar es necesario ubicarse sobre la carpeta que hará las veces de repositorio, esto puede realizarse usando la `Bash Git` y dirigiéndose en ella hacia la carpeta en cuestión, en este caso será `Carpeta_Ejercicio_Git`.

Veamos cómo identificar en qué carpeta se encuentra, cómo navegar hacia otro directorio y crear la carpeta que contendrá al repositorio.

```{bash,include=T}
# Identificar el directorio en el que me encuentro inmediatamente
pwd
```

Una vez identificado el directorio en donde se encuentra puede crear una nueva carpeta, para ello puede usar la siguiente instrucción

```{bash}
# Crear una nueva carpeta
mkdir Carpeta_Ejercicio_Git
```

Esta nueva carpeta se convertirá en repositorio, para ello es preciso moverse hacia su interior y, una vez allí, inicializar `Git`

```{bash}
# Moverse hacia la nueva carpeta
cd Carpeta_Ejercicio_Git
# Identificar la nueva posición 
pwd
# Convertir la carpeta en repositorio Git
git init
```

la instrucción `cd` hace referencia a `change directory` y puede ser utilizada para moverse desde la línea de comandos hacia cualquier otro fichero del ordenador. Para regresar hacia la posición anterior puede utilizar `cd ..`

Una vez realizado este procedimiento deberá recibir un mensaje tipo `Inicializado repositorio Git vacío en`, además de otras lineas con información de contexto, como el nombre de la rama que por defecto se configura, las opciones para cambiarlo y, no menos importante, la sugerencia para configurar la información de credenciales para que `Git` pueda establecer con precisión la autoría de los cambios que a futuro se almacenaran allí.

Realizada la inicialización de `Git` la carpeta empieza a comportarse como un repositorio y tiene las condiciones para empezar a desplegar su estructura, a saber:

  - *Working directory*
  - *Staging area*
  - *Repository*


![**Figura 4**: Estructura de un repositorio `Git`](Imagenes/local_repo_git.png)

Esta estructura cobra sentido en función de su operación, con ello entonces iniciamos el primer ejercicio con `Git`, por ahora usando archivos de texto plano.

# Ejercicio práctico 01: texto plano en `Git`

Cómo acción inicial en un repositorio nuevo es necesario indicarle a `Git` nombre y correo de quien ingresará modificaciones sobre la información almacenada, esto permite que el seguimiento en trabajos colaborativos identifique quién y cuándo se realizaron modificaciones sobre los archivos almacenados. Para ello puede utilizar las siguientes instrucción

```
# Indicarle a Git el correo de usuario
git config --global user.email "you@example.com"

# Indicarle a Git el nombre de usuario
git config --global user.name "Your Name"
```
Una vez en el repositorio inicializado procedamos a crear un primer archivo, para ello es posible emplear la siguiente instrucción en la línea de comandos.

```{bash}
touch Ejemplo01.txt
```

Para este ejemplo puede abrir el fichero e introducir el siguiente texto:

```
Del rigor de la ciencia

*En aquel Imperio, el Arte de la Cartografía logró tal Perfección que el mapa 
de una sola ocupaba toda una Ciudad, y el toda una Provincia. Con el tiempo, 
estos Mapas Desmesurados no satisficieron y los Colegios de Cartógrafos levantaron
un Mapa del Imperio, que tenía el tamaño del Imperio y coincidía puntualmente con él.

Menos Adictas al Estudio de la Cartografía, las Generaciones Siguientes 
entendieron que ese dilatado Mapa era Inútil y no sin Impiedad lo entregaron
a las Inclemencias del Sol y los Inviernos. En los desiertos del Oeste perduran
despedazadas Ruinas del Mapa, habitadas por Animales y por Mendigos; en todo el
País no hay otra reliquia de las Disciplinas Geográficas.

Suárez Miranda, Viajes de Varones Prudentes, Libro Cuarto, Cap. XLV, Lérida, 1658.
```

Una vez copiado y guardado este texto podemos iniciar con el seguimiento a los cambios del archivo en `Git`

```{bash, include=F}
# Copiar texto en nuevo archivo de texto y guardarlo en carpeta
less Borges1.txt >> Ejemplo01.txt

cp Ejemplo01.txt Carpeta_Ejercicio_Git/Ejemplo01.txt 
```


## Incluir archivos en el repositorio


Una vez realizado este procedimiento el archivo se encuentra en el espacio `*Working directory*` y, aunque `Git` ya lo detecta, no se ha iniciado el seguimiento, esto puede hacerlo usando la instrucción `git status`, al ejecutarlo debe recibir un mensaje similar a este.
```{bash, include=F}
git config --global --add safe.directory '/media/DiscoA/SGC/GIT Espacial/Carpeta_Ejercicio_Git'
```


```{bash, echo=F}
cd Carpeta_Ejercicio_Git
git status
```

El seguimiento sobre los archivos inicia con la inclusión de los archivos en el área se `staging`, esto ocupa temporalmente un espacio en memoria `ram` para guardar los cambios antes de ser enviados al repositorio, la instrucción para adherir los archivos a esta área es `git add .`, de esta forma se incluirán todos los archivos nuevos o los cambios sobre estos para iniciar su seguimiento.

***!Importante¡***

Aunque la línea de comandos no entregue ningún mensaje después de la ejecución del *add*, este paso es fundamental para informar a `Git` sobre la inclusión de nuevos archivos o cambios que posteriormente serán formalizados en el repositorio.

```{bash, echo=F}
cd Carpeta_Ejercicio_Git

git add .
```

Al ser esta la primera versión del documento puede ingresar directamente al repositorio, con ello se convertirá en la primera versión, acompañada de la fecha, autor y un comentario que en el futuro ayudará a generar contexto sobre las características con las que se adhiere la información al repositorio. Para ello la instrucción es `git commit`, este se acompaña de un mensaje, precedido de un `-m` siendo esta la abreviación de *message*. 

`git commit -m "Primera versión del texto"`

```{bash, echo=F}
cd Carpeta_Ejercicio_Git
git commit -m"Primera versión del texto"
```

### Primeros cambios sobre el archivo

Una revisión rápida sobre el texto logra identificar varias inconsistencias con respecto al original de *Borges*, todo indica que algunas partes han sido eliminadas y esto cambia sustancialmente el sentido del texto. Para ello se ha dispuesto de una segunda versión, esta vez completamente íntegra frente al original:

```
Del rigor de la ciencia

En aquel Imperio, el Arte de la Cartografía logró tal Perfección que el mapa de
una sola Provincia ocupaba toda una Ciudad, y el mapa del Imperio, toda una 
Provincia. Con el tiempo, estos Mapas Desmesurados no satisficieron y los 
Colegios de Cartógrafos levantaron un Mapa del Imperio, que tenía el tamaño del
Imperio y coincidía puntualmente con él.

Menos Adictas al Estudio de la Cartografía, las Generaciones Siguientes entendieron
que ese dilatado Mapa era Inútil y no sin Impiedad lo entregaron a las Inclemencias
del Sol y los Inviernos. En los desiertos del Oeste perduran despedazadas Ruinas 
del Mapa, habitadas por Animales y por Mendigos; en todo el País no hay otra 
reliquia de las Disciplinas Geográficas.

Suárez Miranda, Viajes de Varones Prudentes, Libro Cuarto, Cap. XLV, Lérida, 1658.
```

Para otorgar fidelidad al texto almacenado en el archivo `Ejemplo01.txt` es necesario entrar al documento y subsanar los errores presentes en el primer párrafo.

```{bash, include=F}
# Copiar texto en nuevo archivo de texto y guardarlo en carpeta
less Borges2.txt >> Ejemplo01.txt

cp Ejemplo01.txt Carpeta_Ejercicio_Git/Ejemplo01.txt 
```

Una vez realizadas estas modificaciones será necesario consultar el estado de los archivos en el repositorio mediante el uso de `git status`.

```{bash, echo=F}
cd Carpeta_Ejercicio_Git
git status
```

Este mensaje indica que efectivamente se ha detectado un cambio sobre el archivo. Una exploración más detallada puede realizarse usando `git diff` en donde se le pedirá al programa identificar y mostrar la diferencias que ha detectado.

Una vez realizada esta exploración será necesario adherir y comentar el archivo para obtener su segunda versión de seguimiento, en este caso se usará como mensaje para el comentario lo siguiente:

`git commit -m "Versión corregida con la original"`

al finalizar es posible usar `git log` para ver el número de versiones que hasta el momento han sido incluidas en el repositorio.

```{bash, echo=F}
cd Carpeta_Ejercicio_Git
git commit -am"Versión corregida con la original"
# Ver número de cambios incluidos
git log
```


Los *commits* son más que mensajes de contexto, al aplicar `git log` habrá notado que cada commit se acompaña de un código extenso, este será la llave para retomar, explorar o revisar una versión anterior de un archivo o de todo un repositorio. Los códigos que acompañan a cada versión son ventanas a los diferentes momentos y cambios que se van dando sobre los archivos en el repositorio. 

Hasta este punto se han implementado algunas de las instrucciones básica en `Git`, todo ello en un repositorio local y sobre una misma *rama*. Dado que la intención es presentar su uso como alternativa para el seguimiento de información espacial, se realizará un segundo ejercicio enfocado para evaluar estas alternativas. Antes de ello es necesario generar una diferenciación sobre *texto plano* e *información binaria*

### Texto plano *Vs* Información binaria

El versionamiento que aplica `Git` está enfocado en texto plano, es decir, aquellos archivos que guardan información sin introducir elementos gráficos o estéticos tales como cambio en tamaño, color o estilo de fuente, margenes, entre otros. Así como el archivo **.txt** es un ejemplo de un texto plano, un documento *Word* cuya extensión es **.docx** es un ejemplo de un archivo *binario*, lo cual indica que las operaciones en máquina que se realizan para observar elementos estéticos sobre el texto demanda de una serie de procesos en el ordenador que ocurren la nivel de **0 y 1**, es decir, en binario. 

Esta situación se repite para imágenes y archivos gráficos, por ende el trabajo con información espacial en `Git` está del lado de los archivos binarios. Ante ello surge una pregunta clave

***¿!No puedo usar `Git` para el seguimiento de información espacial¡?***

Por fortuna, la respuesta es positiva, es posible pero existan algunas restricciones, pues `Git` no será capaz de generar un seguimiento tan detallado como en el archivo de texto, si bien puede decirnos qué archivo cambio, cuando y quien lo modificó, no podrá informar plenamente qué lineas se han modificado exactamente.

Mencionado esto es hora de realizar un ejemplo práctico con información espacial.

# Ejercicio práctico 02: `Git` para la información espacial

La información espacial tiene la característica de concatenar para su fácil manipulación, información temática localizada, esto suele significar que un atributo referente a cualquier temática está situado en una entidad geométrica sobre el espacio geográfico.

Actualmente múltiples formatos son difundidos para el trabajo con información espacial, algunos cuentan con características especiales para determinados entornos. Para el caso del *modelo vectorial* es usual encontrar información en los siguientes formatos.

  - Shapefile (.shp, .shx,.prj,.dbf)
  - Geopackage (.gpkg)
  - Geojason (.geojson)
  - Kml (.kml)
  - Geodatabase (.gdb)

En general estos formatos serán considerados como archivos binarios por `Git`. 

## Seguimiento de shapefiles en `Git`

Para iniciar es necesario [descargar el *shapefile*](https://drive.google.com/file/d/1qN6cCh-glnwxKVkVz3SYl-edpNFMTMLT/view?usp=sharing) que será usado para realizar modificaciones sobre el y hacerle seguimiento a sus versiones al interior del repositorio `Git`. 

Lo primero es descargar la información, crear un nuevo repositorio y en su interior crear un nuevo *shapefile* con geometría polígono, este ultimo será objeto de seguimiento por parte del repositorio.


```
# Crear una nueva carpeta
mkdir Carpeta_Git_Espacial

# Ubicarse en ella
cd Carpeta_Git_Espacial

# Inicializar repositorio
git init
```

Una vez inicializado,despliegue la capa ***Origenes_Carto*** y usando los atributos cree una visualización que le permite identificar y diferenciar los orígenes cartográficos. El resultado de ello tiene que ser similar a esto

![**Figura 3**: Distribución espacial de orígenes cartográficos](Imagenes/OrigenesCol.PNG)

Usando esta información como base, dibuje sobre la capa que creó en el repositorio, el polígono correspondiente al origen central. Una vez realizado debería obtener algo similar a esto.

![**Figura 4**: Polígono creado a partir de orígenes cartográficos](Imagenes/OrigenesCol_NuevoPol.PNG)

Guarde los cambios y, posterior a desactivar el modo de edición, indíquele al repositorio que hay nuevos archivos y estos han sido modificados, para ello use las instrucciones `git status`, verificando que el repositorio notó la inclusión de estos archivos, luego añádalos al **staging** usando `git add .` y, finalmente, añada la información al repositorio usando.

`git commit -m "Creación de polígono único para origen Central"`


El resultado de esta secuencia de comandos debe lucir similar a esto

```
En la rama master

No hay commits todavía

Archivos sin seguimiento:
  (usa "git add <archivo>..." para incluirlo a lo que se será confirmado)
	Origenes.dbf
	Origenes.prj
	Origenes.shp
	Origenes.shx

no hay nada agregado al commit pero hay archivos sin seguimiento presentes (usa "git add" para hacerles seguimiento)
[master (commit-raíz) 65b1327] Creación de polígono único para origen Central
 4 files changed, 1 insertion(+)
 create mode 100644 Origenes.dbf
 create mode 100644 Origenes.prj
 create mode 100644 Origenes.shp
 create mode 100644 Origenes.shx
 ```
 
## Modificando archivos e incluyendolos en `Git`

Al desplegar la nueva capa se encontrará con que la geometría no tiene atributos, cree una nueva columna para calcular allí el perímetro de la geometría recién creada. Al finalizarlo indíquele al repositorio este nuevo cambio y guárdelo como un *commit*, use las siguientes instrucciones para ello. 


```
# Identificar los cambios realizado
git status

# Adherir y enviar los cambios al repositorio
git commit -am "Se agregó nuevo atributo con información de períemtro en Km"

# Verificar que todo se ha guardado correctamente
git status

```
### Crear una nueva rama en `git`

En muchas ocasiones es necesario que distintas personas trabajen sobre un mismo conjunto de datos espaciales, `Git` permite generar flujos paralelos de trabajo mediante la creación de **ramas**. Utilice las siguientes instrucciones para crear una nueva rama y generar modificaciones a la información desde allí.

En primer lugar identifique cuantas ramas existen y desde donde se está almacenando la información.

```
#Identificar cuantas ramas existen
git branch

```

El resultado indica que existe solo una rama por el momento, el `*` que le acompaña indica que es allí en donde se encuentra actualmente.

```
# Crear una nueva rama
git checkout -b ediciones
```
La salida de esta instrucción le indicará que ahora existe una nueva rama de trabajo, esta inicia con el ultimo **commit** que se realizó en **master** y será sobre estos datos que se generará una nueva modificación.

### Modifcando datos en ramas paralelas

Use nuevamente la capa ***Origenes_Carto*** para obtener una referencia y digitalizar  los polígonos correspondientes a los orígenes **Este-Este** y **Oeste-Oeste**. Usando el modo de edición añada estos polígonos al shapefile que se encuentra guardado en el repositorio. El resultado debe ser similar a esto.

![**Figura 4**: Añadir polígonos a la capa](Imagenes/Origenes_V1.png)

Una vez guardados los cambios en la capa, diríjase al repositorio y guarde los cambios en un *commit*.

```
git commit -am "Se añaden nuevos polígonos correspondientes al origen Este-Este y Oeste-Oeste"
```

Una vez guardados estos cambios notará que los nuevos polígonos no tienen ningún valor en la columna **Perímetro**, por ende es necesario actualizar estos valores, guardar la información y realizar un nuevo *commit* indicando la actualización de la información.

AL realizar estos cambios vuelva a la rama **master**, para ello utilice la siguiente instrucción. Recuerde que solo puede desplazarse entre ramas si no tiene información pendiente por agregar mediante el uso de *commits*.

```
#Instrucción para volver a la rama master

git checkout master
```

Estando en la rama *master* notará que nada a cambiado y el shapefile continua solo con el polígono correspondiente al origen central. Todo debe lucir como se expuso en la **figura 3**.

## Git merge

Una vez completas las ediciones paralelas realizadas sobre la capa *Origenes.shp* es posible que se requiera unificar la información para generar un producto final y único. `Git` ofrece la posibilidad de unir las modificaciones paralelas mediante el uso de la instrucción `git merge`.

Antes de ejecutarla conviene explorar la historia de *commits* realizados en el repositorio, esto para las ramas creadas

```
# Ver hitorial de commits
git log
# Ver historial de commits en las ramas actuales
git log --all --graph --decorate --oneline 
```

```{bash,echo=F}
cd Carpeta_Git_Espacial
# Ver historial de commits
git log
# Ver historial de commits en las ramas actuales
git log --all --graph --decorate --oneline
```

realizado esto es importante recordar que el *merge* solo puede aplicarse siempre y cuando **no** existan cambios pendientes por agregar al repositorio en ninguna de las ramas existentes, una vez verificado esto, es posible realizar la operación para reunir la información en una única capa.

Sitúese en la rama **master** y desde allí ejecute la siguiente instrucción

```
git merge ediciones
```

El resultado de esta operación debe lucir similar a este

```
Updating c8f5107..a071318
Fast-forward
 Origenes.dbf | Bin 124 -> 176 bytes
 Origenes.sbn | Bin 132 -> 180 bytes
 Origenes.sbx | Bin 116 -> 132 bytes
 Origenes.shp | Bin 236 -> 508 bytes
 Origenes.shx | Bin 108 -> 124 bytes
 5 files changed, 0 insertions(+), 0 deletions(-)
 
```

En este punto es posible mezclar ramas, crear otros flujos o incluso recuperar los datos del primer commit. El uso de `Git` genera un ambiente controlado para el versionamiento de información, tanto a nivel local como en escenarios de trabajo colaborativo.



